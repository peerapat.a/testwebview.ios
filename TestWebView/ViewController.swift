//
//  ViewController.swift
//  TestWebView
//
//  Created by Peerapat Atawatana on 22/12/20.
//

import UIKit
import WebKit
import AVKit

class ViewController: UIViewController {
   
    lazy var webView: WKWebView = {
        let webConfiguration = WKWebViewConfiguration()
        let webView = WKWebView(frame: .zero, configuration: webConfiguration)
        //webView.uiDelegate = self
        webView.translatesAutoresizingMaskIntoConstraints = false
        return webView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
     
        // Request permission to record.
        AVAudioSession.sharedInstance().requestRecordPermission { [weak self] granted in
            let myURL = URL(string: "https://www.apple.com/")
            let myRequest = URLRequest(url: myURL!)
            DispatchQueue.main.async { [weak self] in
                self?.webView.load(myRequest)
            }
        }
    }
    
    func setupUI() {
        self.view.backgroundColor = .white
        self.view.addSubview(webView)
        
        NSLayoutConstraint.activate([
            webView.topAnchor
                .constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            webView.leftAnchor
                .constraint(equalTo: self.view.safeAreaLayoutGuide.leftAnchor),
            webView.bottomAnchor
                .constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor),
            webView.rightAnchor
                .constraint(equalTo: self.view.safeAreaLayoutGuide.rightAnchor)
        ])
    }
}

